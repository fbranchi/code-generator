/**
 * Created by xps on 05/08/2017.
 */
public class AttributeInfo {

	String type;

	String name;

	Boolean marshaller;

	public AttributeInfo(String type, String name) {
		this.type = type;
		this.name = name;
		marshaller = false;
	}

	public AttributeInfo(String type, String name, Boolean marshaller) {
		this.type = type;
		this.name = name;
		this.marshaller = marshaller;
	}
}
