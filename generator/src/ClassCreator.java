import java.util.List;

/**
 * Created by Flora Branchi on 05/08/2017.
 */
public class ClassCreator {

	private ClassInfo classInfo;
	private List<AttributeInfo> attributes;

	public ClassCreator(ClassInfo classInfo, List<AttributeInfo> attributes) {
		this.classInfo = classInfo;
		this.attributes = attributes;
	}

	/**
	 * Creates class interface.
	 */
	public void createInterface() {
		StringBuilder interfaceText = new StringBuilder();

		addJavaDocHeader(interfaceText);
		createInterfaceHeader(interfaceText);

		System.out.println(interfaceText.toString());
	}

	/**
	 * Creates the base class.
	 */
	public void createBaseClass() {

		StringBuilder baseClassText = new StringBuilder();

		// Add package info
		createPackage(baseClassText);

		// todo: add imports

		// Build class header
		addJavaDocHeader(baseClassText);
		addModelAttributes(baseClassText);
		buildHeader(baseClassText);
		addLine(baseClassText);

		// Create attributes
		createAttributes(baseClassText);
		addLine(baseClassText);

		// Create constructor
		addConstructor(baseClassText);
		addLine(baseClassText);

		// Create getters
		createGetters(baseClassText);

		// Create toString, equals, hash
		createHelperMethods(baseClassText);

		// Create Builder
		createBuilder(baseClassText);

		addLine(baseClassText);
		System.out.println(baseClassText.toString());
	}

	public void createDynamoInterface() {

		StringBuilder dynamoInterface = new StringBuilder();

		addJavaDocHeader(dynamoInterface);
		dynamoInterface.append("interface I").append(classInfo.className)
				.append("DynamoEntity extends I").append(classInfo.className).append(" { \n");

		addLine(dynamoInterface);

		for (AttributeInfo attributeInfo : attributes) {
			String formattedName = toCamelCase(attributeInfo.name);
			String attributeName = removeSpaces(attributeInfo.name);

			dynamoInterface.append("/** \n");
			dynamoInterface.append("* \n ");
			dynamoInterface.append("* The ").append(attributeName).append(" setter. \n");
			dynamoInterface.append("* \n ");
			dynamoInterface.append("* @param ").append(attributeName).append(" the ")
					.append(attributeInfo.name).append("\n");
			dynamoInterface.append("*/ \n ");

			dynamoInterface.append("void set").append(formattedName)
					.append("(final ").append(attributeInfo.type).append(" ").append(attributeName).append("); \n");

			addLine(dynamoInterface);
			addLine(dynamoInterface);
		}

		addClosingBrances(dynamoInterface);

		System.out.println(dynamoInterface.toString());
	}

	public void createDynamoClass() {

		StringBuilder dynamoClass = new StringBuilder();
		addJavaDocHeader(dynamoClass);
		createDynamoHeader(dynamoClass);
		addLine(dynamoClass);
		createDyanamoAttributes(dynamoClass);
		addLine(dynamoClass);
		createDynamoGettersAndSetters(dynamoClass);
		addLine(dynamoClass);

		createDynamoFromModel(dynamoClass);
		addLine(dynamoClass);
		createDynamoToModel(dynamoClass);

		addClosingBrances(dynamoClass);

		System.out.println(dynamoClass.toString());

	}

	private void createDynamoFromModel(StringBuilder dynamoClass) {
		dynamoClass.append("@Override\n");
		dynamoClass.append("  public void fromModel(final ").append(classInfo.className)
				.append(" model) throws DynamoDbDatastoreException { \n");
		dynamoClass.append("super.fromModel(model); \n");

		for (AttributeInfo attributeInfo : attributes) {
			String formattedName = toCamelCase(attributeInfo.name);
			String attributeName = removeSpaces(attributeInfo.name);

			dynamoClass.append("set").append(formattedName).append("(model.get").append(formattedName)
					.append("()); \n");
		}
		addClosingBrances(dynamoClass);
		addLine(dynamoClass);
	}

	private void createDynamoToModel(StringBuilder dynamoClass) {
		dynamoClass.append("@Override\n");
		dynamoClass.append("  public ").append(classInfo.className)
				.append(" toModel() throws DynamoDbDatastoreException { \n");
		dynamoClass.append("return new ").append(classInfo.className).append(".Builder()\n");
		dynamoClass.append(".from(super.toModel()) \n");

		for (AttributeInfo attributeInfo : attributes) {
			String formattedName = toCamelCase(attributeInfo.name);
			String attributeName = removeSpaces(attributeInfo.name);

			dynamoClass.append(".with").append(formattedName).append("(get").append(formattedName)
					.append("()) \n");
		}
		dynamoClass.append(" .build(); \n");
		addClosingBrances(dynamoClass);
		addLine(dynamoClass);

	}

	private void createDynamoHeader(StringBuilder dynamoClass) {
		dynamoClass.append("public class ").append(classInfo.className)
				.append("DynamoEntity extends ").append(classInfo.extendsClass)
				.append("DynamoEntity<").append(classInfo.className).append(".Builder, ")
				.append(classInfo.className)
				.append("> implements I").append(classInfo.className).append("DynamoEntity")
				.append(" { \n");
	}

	private void createDyanamoAttributes(StringBuilder dynamoClass) {

		for (AttributeInfo attribute : attributes) {
			String attributeName = removeSpaces(attribute.name);
			dynamoClass.append("private ").append(attribute.type).append(" ").append(attributeName);
			addComma(dynamoClass);
		}
	}

	private void createDynamoGettersAndSetters(StringBuilder dynamoClass) {
		for (AttributeInfo attribute : attributes) {
			String formattedName = toCamelCase(attribute.name);
			String attributeName = removeSpaces(attribute.name);

			dynamoClass.append("@Override \n");
			dynamoClass.append("@DynamoDBAttribute(attributeName = \"").append(attributeName)
					.append("\") \n");
			dynamoClass.append("public ").append(attribute.type).append(" get").append(formattedName).append("() { \n");
			dynamoClass.append("return ").append(attributeName);
			addComma(dynamoClass);
			addClosingBrances(dynamoClass);
			addLine(dynamoClass);
			addLine(dynamoClass);

			dynamoClass.append("@Override \n");
			dynamoClass.append("public void set").append(formattedName).append("(")
					.append("final ").append(attribute.type).append(" ").append(attributeName).append(")")
					.append(" { \n");
			dynamoClass.append("this.").append(attributeName).append(" = ").append(attributeName)
					.append("; \n");
			dynamoClass.append(" } \n");
			addLine(dynamoClass);

		}
	}

	private void createBuilder(StringBuilder stringBuilder) {
		createBuilderHeader(stringBuilder);
		addLine(stringBuilder);
		createBuilderAttributes(stringBuilder);
	}

	private void createBuilderHeader(StringBuilder stringBuilder) {
		stringBuilder.append("/** \n");
		stringBuilder.append("* Class builder. \n");
		stringBuilder.append("*/ \n");
		stringBuilder.append("public static final class Builder extends ")
				.append(classInfo.extendsClass)
				.append(".Builder<").append(classInfo.className).append(".Builder, ")
				.append(classInfo.className).append("> { \n");
	}

	private void createBuilderAttributes(StringBuilder stringBuilder) {

		for (AttributeInfo attributeInfo : attributes) {
			String formattedName = toCamelCase(attributeInfo.name);
			String attributeName = removeSpaces(attributeInfo.name);
			stringBuilder.append("/** \n");
			stringBuilder.append("* Add the ").append(attributeInfo.name).append(" to the builder.\n");
			stringBuilder.append("* \n");
			stringBuilder.append("* @param ").append(attributeName).append(" the ")
					.append(attributeInfo.name).append("\n");
			stringBuilder.append("* @return the modified builder object \n");
			stringBuilder.append("*/");
			addLine(stringBuilder);
			stringBuilder.append("public Builder with").append(formattedName)
					.append("(final ").append(attributeInfo.type).append(" ")
					.append(attributeName).append(")").append(" { \n");

			stringBuilder.append("instance.").append(attributeName).append(" = ").append(attributeName);
			addComma(stringBuilder);
			stringBuilder.append("return this; \n");
			addClosingBrances(stringBuilder);
			addLine(stringBuilder);
		}

		stringBuilder.append("\n }");
	}

	/**
	 * Adds package to beginning of file.
	 */
	private void createPackage(StringBuilder stringBuilder) {
		stringBuilder.append("package ").append(classInfo.classPackage);
		addComma(stringBuilder);
		addLine(stringBuilder);
	}

	private void createInterfaceHeader(StringBuilder interfaceText) {

		interfaceText.append("public interface I").append(classInfo.className);
		if (classInfo.extendsClass != null) {
			interfaceText.append(" extends ").append("I").append(classInfo.extendsClass);
		}

		addStartingBraces(interfaceText);
	}

	/**
	 * Build class header.
	 *
	 * @param baseClassText the String builder to be add to.
	 */
	private void buildHeader(StringBuilder baseClassText) {
		baseClassText.append("public class ").append(classInfo.className);

		// Add extends
		if (classInfo.extendsClass != null) {
			baseClassText.append(" extends ").append(classInfo.extendsClass);
		}

		// Adds IBaseClass
		baseClassText.append(" implements I").append(classInfo.className);
		addStartingBraces(baseClassText);
		addLine(baseClassText);
	}

	private void addConstructor(StringBuilder stringBuilder) {
		stringBuilder.append("private ").append(classInfo.className).append("() { \n");
		stringBuilder.append("super(); \n ");
		stringBuilder.append("} \n");
	}

	private void createHelperMethods(StringBuilder baseClassText) {
		baseClassText.append(" // Must add hashCode, toString and equals \n \n");
	}


	private void createAttributes(StringBuilder baseClassText) {
		for (AttributeInfo attribute : attributes) {

			addJavaDocSpace(baseClassText);
			addVerifiers(baseClassText, attribute);
			String attributeName = removeSpaces(attribute.name);
			baseClassText.append("private ").append(attribute.type).append(" ").append(attributeName);

			addComma(baseClassText);

			addLine(baseClassText);
		}
	}

	private void createGetters(StringBuilder baseClassText) {
		for (AttributeInfo attribute : attributes) {

			String formattedName = toCamelCase(attribute.name);
			String attributeName = removeSpaces(attribute.name);

			baseClassText.append("/** \n");
			baseClassText.append("* ").append(formattedName).append(" getter. \n");
			baseClassText.append("* \n ");
			baseClassText.append("* @return the ").append(attributeName).append("\n");
			baseClassText.append("*/ \n ");
			baseClassText.append("@Override \n");

			baseClassText.append("public ").append(attribute.type).append(" get").append(formattedName)
					.append("()");
			addStartingBraces(baseClassText);

			baseClassText.append("return ").append(attributeName);
			addComma(baseClassText);
			addClosingBrances(baseClassText);

			addLine(baseClassText);
			addLine(baseClassText);
		}
	}

	private void addStartingBraces(StringBuilder stringBuilder) {
		stringBuilder.append(" { \n");
	}

	public void addClosingBrances(StringBuilder stringBuilder) {
		stringBuilder.append(" } ");
	}

	public void addComma(StringBuilder stringBuilder) {
		stringBuilder.append("; \n");
	}

	public void addLine(StringBuilder stringBuilder) {
		stringBuilder.append("\n");
	}

	static String toCamelCase(String s) {
		String[] parts = s.split(" ");
		String camelCaseString = "";
		for (String part : parts) {
			camelCaseString = camelCaseString + toProperCase(part);
		}
		return camelCaseString;
	}

	static String toProperCase(String s) {
		return s.substring(0, 1).toUpperCase() +
				s.substring(1).toLowerCase();
	}

	static String removeSpaces(String string) {
		return string.replaceAll("\\s", "");
	}

	private void addJavaDocSpace(StringBuilder stringBuilder) {
		stringBuilder.append("/** \n");
		stringBuilder.append("* \n");
		stringBuilder.append("*/ \n ");
	}

	private void addJavaDocHeader(StringBuilder stringBuilder) {
		stringBuilder.append("/** \n");
		stringBuilder.append("* ").append(classInfo.className).append(" model. \n");
		stringBuilder.append("* \n");
		stringBuilder.append("* @author Flora Branchi (flora.branchi@aquiris.com.br) \n");
		stringBuilder.append("* @since \n");
		stringBuilder.append("*/ \n ");
	}

	private void addModelAttributes(StringBuilder stringBuilder) {
		stringBuilder.append("@ModelAttributes(typeName =\"").append(classInfo.className)
				.append("\", typeVersion = 1)");
		addLine(stringBuilder);
	}

	private void addHash(StringBuilder stringBuilder, AttributeInfo attributeInfo) {
		// add to verify if reward and add hash annotation
		if (attributeInfo.type == "Reward") {
			stringBuilder.append("@NotEmpty(state = {EValidateState.ANY}) \n");
		}
	}

	/**
	 * Adds null and empty verifiers to attribute info.
	 *
	 * @param stringBuilder stringBuilder
	 * @param attributeInfo attributeInfo
	 */
	private void addVerifiers(StringBuilder stringBuilder, AttributeInfo attributeInfo) {

		// or list
		stringBuilder.append("@NotNull(state = {EValidateState.ANY}) \n");
		if (attributeInfo.type == "String") {
			stringBuilder.append("@NotEmpty(state = {EValidateState.ANY}) \n");
		}
	}

}
