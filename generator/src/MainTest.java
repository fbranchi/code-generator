import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xps on 05/08/2017.
 */
public class MainTest {

	@Test
	public void test() {

		ClassInfo classInfo = new ClassInfo("PlayerCampaignStageStatus", "AbstractPlayerItem");

		List<AttributeInfo> attributeInfoList = new ArrayList<>();
		AttributeInfo attributeInfo1 = new AttributeInfo("Integer", "completions In Period");
		AttributeInfo attributeInfo2 = new AttributeInfo("Integer", "total Tries ");
		AttributeInfo attributeInfo3 = new AttributeInfo("Integer", "maximum Star Count");
		attributeInfoList.add(attributeInfo1);
		attributeInfoList.add(attributeInfo2);
		attributeInfoList.add(attributeInfo3);

		ClassCreator classCreator = new ClassCreator(classInfo, attributeInfoList);
		classCreator.createBaseClass();
		classCreator.createInterface();
		classCreator.createDynamoClass();
		classCreator.createDynamoInterface();


	}

}