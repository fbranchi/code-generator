/**
 * Created by xps on 05/08/2017.
 */
public class ClassInfo {

	String extendsClass;

	String className;

	String classPackage;

	public ClassInfo(String className) {
		this.className = className;
		extendsClass = null;
		classPackage = "package.com";
	}

	public ClassInfo(String className, String extendsClass) {
		this.extendsClass = extendsClass;
		this.className = className;
		classPackage = "package.com";
	}

	public ClassInfo(String extendsClass, String className, String classPackage) {
		this.extendsClass = extendsClass;
		this.className = className;
		this.classPackage = classPackage;
	}
}
